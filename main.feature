	Feature:healthhub

	@install	
	Scenario:Registration Login Successful registration and login
			Then I Launch the application
			When I enter UN "7718809884"
			Then I click Get Otp
			Then I sleep for 1 sec
			Then I click Imageprofile
			Then I click the editprofile button
			When I enter LastName "mishra"
			Then I click swipe downward
			Then I click Save
			Then I click press_back_button
	@continue
	Scenario:HHR11-HHR20_Verify the functionality of My Health Data section of HH app
		Then I sleep for a moment
		Then I wait for the My Health Data Element
		Then I wait for Medical Bills Element
		Then I sleep for a moment
		Then I click press_back_button
		Then I wait for Others Element
		Then I sleep for a moment
		Then I click press_back_button
		Then I wait for Medical Images Element
		Then I sleep for a moment
		Then I click press_back_button
		Then I click press_back_button
		
	@continue
	Scenario:HHR116, HHR117, HHR118, HHR119_Verify that user can enter and update allergy details in Profile section in HealthHub
			Then I sleep for 1 sec
			Then I wait for the My Health Data Element
			Then I wait for Medical Images Element
			Then I click Medical Images Element1
			Then I sleep for 5 sec
			Then I wait for Add option Medical Images Element 
			Then I click press_back_button
			Then I click press_back_button
			Then I click press_back_button
		

	@continue
	Scenario:HHR87, HHR88, HHR89_Verify the record sharing functionality with a single contact
			Then I sleep for 5 sec
			Then I wait for Shared By Me
			Then I click on Pranjal
			Then I click on photo Pranjal
			Then I click on share button
			Then I click on share Pranjal
			Then I click on Record Share
			Then I click on sharing button
			Then I click press_back_button
			Then I click press_back_button
			Then I click press_back_button
			
	@continue
	Scenario:HHR-71,72 & 73_Verify the download functionality of Health hub.
			Then I sleep for 5 sec
			Then I wait for the My Health Data Element
			Then I wait for Others Element
			Then I click Others images Reports Element
			Then I wait for Add option Medical Images Element
			Then I click on Download
	@continue
	Scenario:HHR87_Verify the record sharing functionality with multiple contacts.
            Then I wait for the My Health Data Element
			Then I wait for Others Element
			Then I click Others images Reports Element
			Then I wait for Add option Medical Images Element
			Then I click on Share 
			Then I click on Dr Pranjal
			Then I click on confirm share
			Then I wait for the sharing message
			Then I click press_back_button
			Then I click press_back_button
			Then I click press_back_button
			
	@continue
	Scenario:700_Registration Login Successful registration and login
			Then I click Imageprofile
			Then I click the editprofile button
			When I enter LastName "mishra"
			Then I click swipe downward
			Then I click Save
			Then I click press_back_button
				
	@continue
	Scenario:800_Verify the delete functionality(both single and multiple).
			Then I wait for Shared By Me
			Then I click on Pranjal
			Then I click on delete Pranjal
			Then I click on Unshare Pranjal
			Then I click on Yes
			Then I click press_back_button
			
	@continue
	Scenario:HHR-74_Verify the delete functionality(both single and multiple).
			Then I sleep for 5 sec
			Then I wait for the My Health Data Element
			Then I click Lab Reports
			Then I click on delete Lab Reports
			Then I click on Yes Lab Reports
			Then I click on Picture Lab Reports 
			Then I click press_back_button
			Then I click press_back_button
			
	@continue
	Scenario:900_Verify the Profile page elements present and edit the same
			Then I sleep for 1 sec
			Then I click Imageprofile
			Then I click the editprofile button
			Then I click swipe downward
			Then I click Save
			Then I click press_back_button
			
	@continue
	Scenario:HHR8, HHR9, HHR10, HHR11_Verify the fields present on the basic profile screens.
            Then I sleep for 1 sec
			Then I click Imageprofile
			Then I wait for User Profile
			Then I wait for Health Card
			Then I wait for Height
			Then I wait for Weight
			Then I wait for Contact
			Then I wait for Medical Insurance
			Then I wait for Provider
			Then I wait for Policy No
			Then I wait for Emergency Contact
			Then I click press_back_button
			
	@continue
	Scenario:1001_the function with take photo
		Then I sleep for a moment
		Then I wait for the My Health Data Element
		Then I wait for Medical Images Element
		Then I click press_back_button
		Then I click press_back_button
		
	@continue
	Scenario: HHR78, HHR79_Verify that the user can add one or more ID wallet to his HH account
        Then I sleep for 5 sec
		Then I swipe over first time
		Then I wait for Sync Account
		Then I click ID wallet Add Button
	    Then I click ID wallet Add New
		When I enter UN8 "MV-117692"
		When I enter UN9 "health123"
		Then I click ID wallet SAVE
		Then I sleep for 5 sec
		Then I wait for SRL Diagnostics verbiage
		Then I click press_back_button
		Then I swap backward
		
	@continue
	Scenario:HHR82_Verify the ID wallet edit function
		Then I swipe over first time
		Then I wait for Sync Account
	    Then I click Edit wallet
		Then I click Add SAVE
		Then I click press_back_button
		Then I swap backward
		
		
	@continue
	Scenario:HHR82 _Verify the ID wallet delete function
		Then I swipe the screen one time
		Then I wait for Sync Account
		Then I click ID Delete
		Then I click Yes ID Delete
		Then I sleep for 5 sec
		Then I click press_back_button
		Then I swap backward
		
	@continue
	Scenario:1601_Heath Charts Weight
		Then I wait for Health Charts
		Then I click on Edit Health Charts
		Then I enter "333" into input field number 1
	    Then I click on datepicker
		Given I set the date to "11-09-2012" on DatePicker with index 1
		Then I click on Set
		Then I click on timepicker
		Given I set the time to "11:15" on TimePicker with index 1
		Then I click on Cancel
		Then I click on SAVE
		Then I sleep for 1 sec
		Then I click press_back_button
		Then I click press_back_button
		

	@continue
	Scenario:1701_verify that user is  able to record health parameters and view the same using charts
		Then I wait for Health Charts
		Then I Search for Weight
		Then I Search for Blood Pressure-Systolic
		Then I Search for Blood Pressure-Diastolic
		Then I Search for Serum Cholesterol
		Then I click swipe downward
		Then I Search for Serum Lipase
		Then I Search for Serum Amylase
		Then I sleep for 1 sec
		Then I click press_back_button
		
		
	@continue
	Scenario:1801_Verify that user can enter and update allergy details in Profile section in HealthHub swaping problem
			Then I sleep for 5 sec
			Then I swipe over first time
			Then I wait for Medical History
			Then I click for FOOD ALLERGIES
			Then I click for Egg
			Then I click for Fruit
			Then I click for saveTV
			Then I click for DRUG ALLERGIES
			Then I click for Vaccines
			Then I click for Penicillin
			Then I click for saveTV
			Then I click for ENVIRONMENTAL ALLERGIES
			Then I click for Smoke
			Then I click for Pollen
			Then I click for saveTV
			Then I click for FAMILY HISTORY
			Then I click for Asthma
			Then I click for Cancer
			Then I click for saveTV
			Then I click for KNOWN CONDITIONS
			Then I click for Diabetes
			Then I click for Mental illness
			Then I click for saveTV
			Then I click for GENETIC DISORDERS
			Then I click for Colour Blindness
			Then I click for Cystic Fibrosis
			Then I click for saveTV
			Then I click press_back_button
			Then I swap backward
			
	@continue
	Scenario:HHR-66_Verify the capture and upload functionality of Health hub.
        Then I sleep for a moment
		Then I wait for the My Health Data Element
		Then I wait for Medical Images Element
		Then I click addition button
		Then I select the photo
		Then I select gallery photo
		Then I click the tick of upload
		
		

			
			
			
			
			
			
			

			


			
			
			
			
			
			



	   
		
			
			
			
			
			
			
			
		

